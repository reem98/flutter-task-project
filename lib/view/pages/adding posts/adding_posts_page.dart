import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';
import 'package:task_project_flutter/logic/Controllers/adding%20posts/adding_posts_controller.dart';
import 'package:task_project_flutter/view/compont/custom%20loading/custom_loading_indicator_widget.dart';
import 'package:task_project_flutter/view/compont/custom%20texts/big_text.dart';

import '../../../utils/colors.dart';
import '../../compont/custominput text form/input_text_form_field.dart';
import '../../compont/customs buttons/custom_button.dart';
import '../../sections widget in pages/adding posts page/take_image_widget.dart';

class AddingPostsPage extends StatelessWidget {
  const AddingPostsPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.mainColor,
      body: GetBuilder<AddingPostsController>(builder: (controller) {
        return controller.isLoad
            ? const CustomLoadingIndicatorWidget()
            : Container(
                margin: EdgeInsets.only(left: 4.w, right: 4.w, top: 6.h),
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      BigText(
                        text: 'Share us your idea 😊',
                        size: 16.sp,
                        color: AppColors.textColor,
                      ),
                      BigText(
                        text: 'By posting what do you want',
                        size: 16.sp,
                      ),
                      Container(
                          alignment: Alignment.center,
                          child: takeImageLayout('Add Image', controller,
                              Get.context, double.infinity, 24.h)),
                      SizedBox(
                        height: 5.h,
                      ),
                      InputTextFormField(
                          hintText: 'name',
                          textEditingController: controller.nameController),
                      SizedBox(
                        height: 2.h,
                      ),
                      InputTextFormField(
                        hintText: 'description',
                        textEditingController: controller.descriptionController,
                        maxLine: 3,
                      ),
                      SizedBox(
                        height: 6.h,
                      ),
                      Align(
                        alignment: Alignment.center,
                        child: CustomButton(
                          onPressed: () {
                            controller.uploadPost();
                          },
                          buttonText: 'Upload',
                          fontSize: 14,
                          width: 32.w,
                          height: 6.h,
                        ),
                      ),
                    ],
                  ),
                ),
              );
      }),
    );
  }
}
