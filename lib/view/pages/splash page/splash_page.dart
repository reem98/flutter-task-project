import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';
import 'package:task_project_flutter/routes/routes.dart';
import 'package:task_project_flutter/utils/app_constants.dart';
import 'package:task_project_flutter/view/compont/custom%20texts/big_text.dart';
import 'package:task_project_flutter/view/compont/customs%20buttons/custom_button.dart';

import '../../../logic/Controllers/home posts/home_posts_controller.dart';
import '../../compont/custom texts/small_text.dart';

class SplashPage extends StatelessWidget {
  const SplashPage({super.key});
  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      body: Container(
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/images/camera.png'),
            fit: BoxFit.fill
            )
        ),
        child:Container(
          margin: EdgeInsets.symmetric(horizontal: 6.w,vertical:7.h ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            
            children: [
            Row(
              children: [
                BigText(text: 'Poto',size: 30.sp,),
              ],
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                BigText(text: 'Explore Your Capture',size: 28.sp,),
                SizedBox(
                  height: 2.h,
                ),
                SmallText(text: 'Share your capture with',size: 25.sp,),
                SmallText(text: 'Poto gallery.',size: 25.sp,),
                 SizedBox(
                  height: 2.h,
                ),
                Center(
                  child: CustomButton(
                    buttonText: 'Get Started',
                    width: 50.w,
                    radius: 40,
                    transparent: true,
                   onPressed: () async{
                    await Get.find<HomePostsController>().prefs.setString(AppConstants.IS_SHOWN_SPLASH, AppConstants.IS_SHOWN_SPLASH);
                    Get.offAllNamed(AppRoutes.mainBottomNavigationBar);
                   },
                    ),
                ),
              ],
            ),
          ],),
        )
      ),
    );
  }
}