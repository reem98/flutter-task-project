import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:sizer/sizer.dart';
import 'package:task_project_flutter/logic/Controllers/home%20posts/home_posts_controller.dart';
import 'package:task_project_flutter/utils/colors.dart';
import 'package:task_project_flutter/view/compont/custom%20app%20icons/app_icons.dart';
import 'package:task_project_flutter/view/compont/custom%20loading/custom_loading_indicator_widget.dart';
import 'package:task_project_flutter/view/compont/custom%20texts/big_text.dart';
import 'package:task_project_flutter/view/sections%20widget%20in%20pages/home%20page/head_section.dart';
import 'package:task_project_flutter/view/sections%20widget%20in%20pages/home%20page/slider_section.dart';

import '../../../utils/app_constants.dart';


class HomePage extends StatelessWidget {
  const HomePage({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.mainColor,
      body: Container(
          margin: EdgeInsets.only(left: 4.w, right: 4.w, top: 2.h),
          child: GetBuilder<HomePostsController>(builder: (homeController) {
            return homeController.isLoadAll || homeController.isLoadPopular
                ? const CustomLoadingIndicatorWidget()
                : ListView(
                    children: [
                      const HeadSection(),
                      SliderSectionWidget(),
                      SizedBox(
                        height: 2.h,
                      ),
                      BigText(
                        text: "Today's Capture",
                        size: 20.sp,
                      ),
                      StaggeredGridView.countBuilder(
                        shrinkWrap: true,
                        physics: const NeverScrollableScrollPhysics(),
                        crossAxisCount: 2,
                        itemCount: homeController.allPostsModel.posts!.length,
                        itemBuilder: (BuildContext context, int index) {
                          final dimensions = homeController
                              .allPostsModel.posts![index].dimensions;
                          final dimensionParts = dimensions!.split('x');
                          final width =
                              double.tryParse(dimensionParts[0]) ?? 1.0;
                          final height =
                              double.tryParse(dimensionParts[1]) ?? 1.0;
                          return AspectRatio(
                            aspectRatio: width / height,
                            child: Container(
                              margin: const EdgeInsets.all(6),
                              decoration: BoxDecoration(
                                  color: Colors.grey.shade800,
                                  borderRadius: BorderRadius.circular(30),
                                  image: DecorationImage(
                                      fit: BoxFit.fill,
                                      image: NetworkImage(
                                          '${AppConstants.BASE_URL}${homeController.allPostsModel.posts![index].image}'))),
                              child: Container(
                                  margin: EdgeInsets.symmetric(
                                      horizontal: 2.w, vertical: 1.h),
                                  alignment: Alignment.topRight,
                                  child: homeController.allPostsModel
                                              .posts![index].isLiked ==
                                          true
                                      ? const AppIcons(
                                          icon: Icons.favorite,
                                          containerSize: 28,
                                          iconColor: Colors.red,
                                        )
                                      : const AppIcons(
                                          icon: Icons.favorite_border,
                                          containerSize: 28,
                                          backgruondcolor: AppColors.white,
                                          iconColor: Colors.red,
                                        )),
                            ),
                          );
                        },
                        staggeredTileBuilder: (int index) {
                          final dimensions = homeController
                              .allPostsModel.posts![index].dimensions;
                          final dimensionParts = dimensions!.split('x');
                          final width =
                              double.tryParse(dimensionParts[0]) ?? 1.0;
                          final height =
                              double.tryParse(dimensionParts[1]) ?? 1.0;

                          return StaggeredTile.extent(
                              1,
                              height /
                                  width *
                                  25.h);
                        },
                        mainAxisSpacing: 8,
                        crossAxisSpacing: 8,
                      )
                    ],
                  );
          })),
    );
  }
}
