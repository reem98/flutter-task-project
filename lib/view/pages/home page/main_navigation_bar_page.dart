import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:persistent_bottom_nav_bar/persistent_tab_view.dart';
import 'package:task_project_flutter/view/pages/home%20page/home_page.dart';

import '../../../utils/colors.dart';
import '../adding posts/adding_posts_page.dart';

class MainBottomNavigationBarPage extends StatelessWidget {
  const MainBottomNavigationBarPage({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {

    List<Widget> _buildScreens() {
      return [
        HomePage(),
        Scaffold(
          backgroundColor: AppColors.mainColor,
        ),
        AddingPostsPage(),
        Scaffold(
          backgroundColor: AppColors.mainColor,
        ),
        Scaffold(
          backgroundColor: AppColors.mainColor,
        ),
      ];
    }
    List<PersistentBottomNavBarItem> _navBarsItems() {
      return [
        PersistentBottomNavBarItem(
          icon: const Icon(Icons.auto_awesome_mosaic),
          activeColorPrimary: AppColors.white,
          inactiveColorPrimary: CupertinoColors.systemGrey,
        ),
        PersistentBottomNavBarItem(
          icon: const Icon(Icons.search),
          activeColorPrimary: AppColors.white,
          inactiveColorPrimary: CupertinoColors.systemGrey,
        ),
        PersistentBottomNavBarItem(
          icon: const Icon(
            Icons.add,
            color: AppColors.white,
          ),
          activeColorPrimary: AppColors.secondColor,
          inactiveColorPrimary: CupertinoColors.systemGrey,
        ),
        PersistentBottomNavBarItem(
          icon: const Icon(Icons.bookmark_outlined),
          activeColorPrimary: AppColors.white,
          inactiveColorPrimary: CupertinoColors.systemGrey,
        ),
        PersistentBottomNavBarItem(
          icon: const Icon(Icons.person),
          activeColorPrimary: AppColors.white,
          inactiveColorPrimary: CupertinoColors.systemGrey,
        ),
      ];
    }

    return PersistentTabView(context,
        screens: _buildScreens(),
        items: _navBarsItems(),
        confineInSafeArea: true,
        backgroundColor: Colors.black, 
        resizeToAvoidBottomInset:
            true, 
        hideNavigationBarWhenKeyboardShows:
            true,
        decoration: NavBarDecoration(
          borderRadius: BorderRadius.circular(10.0),
          colorBehindNavBar: Colors.black,
        ),
        popAllScreensOnTapOfSelectedTab: true,
        popActionScreens: PopActionScreensType.all,
        itemAnimationProperties: const ItemAnimationProperties(
          duration: Duration(milliseconds: 200),
          curve: Curves.ease,
        ),
        screenTransitionAnimation: const ScreenTransitionAnimation(
          animateTabTransition: true,
          curve: Curves.ease,
          duration: Duration(milliseconds: 200),
        ),
        navBarStyle: NavBarStyle.style15);
  }
}
