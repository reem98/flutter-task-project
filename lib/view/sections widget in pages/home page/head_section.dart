import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

import '../../../utils/colors.dart';
import '../../compont/custom texts/big_text.dart';

class HeadSection extends StatelessWidget {
  const HeadSection({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            BigText(
              text: "Lets's share",
              size: 20.sp,
            ),
            const Icon(
              Icons.notifications,
              color: AppColors.textColor,
            )
          ],
        ),
        Row(
          children: [
            BigText(
              text: "your ",
              size: 20.sp,
            ),
            BigText(
              text: "comment",
              size: 20.sp,
              color: AppColors.textColor,
            ),
          ],
        ),
      ],
    );
  }
}
