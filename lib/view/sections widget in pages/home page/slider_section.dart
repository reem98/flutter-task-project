import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';
import 'package:task_project_flutter/logic/Controllers/home%20posts/home_posts_controller.dart';

import '../../../utils/app_constants.dart';
import '../../../utils/colors.dart';
import '../../compont/custom texts/small_text.dart';

class SliderSectionWidget extends StatelessWidget {
  const SliderSectionWidget({
    super.key,
  });
  @override
  Widget build(BuildContext context) {
    return CarouselSlider.builder(
      options: CarouselOptions(
        height: 30.h,
        aspectRatio: 16 / 9,
        viewportFraction: 1,
        initialPage: 0,
        enableInfiniteScroll: true,
        reverse: false,
        autoPlayInterval: const Duration(seconds: 3),
        autoPlayAnimationDuration: const Duration(milliseconds: 800),
        autoPlayCurve: Curves.fastOutSlowIn,
        enlargeCenterPage: true,
        scrollDirection: Axis.horizontal,
      ),
      itemCount:
          Get.find<HomePostsController>().popularSliderModel.posts!.length,
      itemBuilder: (BuildContext context, int itemIndex, int pageViewIndex) =>
          Container(
        decoration: BoxDecoration(
            color: Colors.grey.shade800,
            borderRadius: BorderRadius.circular(50),
            image: DecorationImage(
                fit: BoxFit.fill,
                image: NetworkImage(
                    '${AppConstants.BASE_URL}${Get.find<HomePostsController>().popularSliderModel.posts![itemIndex].image}'))),
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 7.w, vertical: 3.h),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              CircleAvatar(
                radius: 25,
                backgroundColor: AppColors.textColor,
                backgroundImage: NetworkImage(
                    '${AppConstants.BASE_URL}${Get.find<HomePostsController>().popularSliderModel.posts![itemIndex].image}'),
              ),
              SizedBox(
                width: 2.w,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SmallText(
                    text: Get.find<HomePostsController>()
                        .popularSliderModel
                        .posts![itemIndex]
                        .name
                        .toString(),
                    color: AppColors.white,
                    fontWeight: FontWeight.bold,
                  ),
                  SmallText(
                    text: Get.find<HomePostsController>()
                        .popularSliderModel
                        .posts![itemIndex]
                        .description
                        .toString(),
                    color: AppColors.white,
                    fontWeight: FontWeight.bold,
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
