import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

import '../../../utils/colors.dart';
import '../../compont/custom app icons/app_icons.dart';
import '../../compont/custom texts/big_text.dart';

showBottomSheetImageWidget(context, dynamic controller) {
  double width = MediaQuery.of(context).size.width;
  return showModalBottomSheet(
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(20),
        ),
      ),
      clipBehavior: Clip.antiAliasWithSaveLayer,
      context: context,
      builder: (context) {
        return Container(
          padding: EdgeInsets.fromLTRB(5.w, 2.h, 1.w, 1.h),
          height: 200,
          decoration: const BoxDecoration(
            color: AppColors.mainColor,
          ),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                BigText(
                  text: "Please Choose way",
                  size: 19.sp,
                  fontWeight: FontWeight.w500,
                  color: AppColors.white,
                ),
                Container(
                  margin: const EdgeInsets.only(
                      left: 4, right: 4, top: 10, bottom: 10),
                  height: 1,
                  width: width,
                  color: Colors.white,
                ),
                InkWell(
                  onTap: () async {
                    controller.gallery();
                  },
                  child: Container(
                      width: double.infinity,
                      padding: EdgeInsets.fromLTRB(1.w, 2.h, 1.w, 1.h),
                      child: Row(
                        children: [
                          const AppIcons(
                            icon: Icons.perm_media_outlined,
                            iconColor: AppColors.mainColor,
                            iconSize: 25,
                          ),
                          SizedBox(width: 5.w),
                          BigText(
                            text: "From Gallery",
                            fontWeight: FontWeight.normal,
                            color: AppColors.white,
                            size: 19.sp,
                          )
                        ],
                      )),
                ),
                InkWell(
                  onTap: () async {
                    controller.camera();
                  },
                  child: Container(
                      width: double.infinity,
                      padding: EdgeInsets.fromLTRB(1.w, 2.h, 1.w, 1.h),
                      child: Row(
                        children: [
                          const AppIcons(
                            icon: Icons.camera_alt,
                            iconColor: AppColors.mainColor,
                            iconSize: 25,
                          ),
                          SizedBox(width: 5.w),
                          BigText(
                              text: "From Camera",
                              fontWeight: FontWeight.normal,
                              color: AppColors.white,
                              size: 19.sp)
                        ],
                      )),
                ),
              ],
            ),
          ),
        );
      });
}
