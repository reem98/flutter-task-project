import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:task_project_flutter/view/compont/custom%20app%20icons/app_icons.dart';
import 'package:task_project_flutter/view/compont/custom%20texts/big_text.dart';

import '../../../utils/colors.dart';

Widget takeImageLayout(
    String text, dynamic controller, context, double width, double height) {
  return controller.imageName == ''
      ? GestureDetector(
          onTap: () async {
            controller.requestPermisionForPhotos(controller, context);
          },
          child: Column(
            children: [
              const SizedBox(
                  child: Icon(
                Icons.add_a_photo,
                size: 90,
                color: AppColors.mainColor2,
              )),
              BigText(
                color: AppColors.white,
                text: text,
                size: 15.sp,
              )
            ],
          ),
        )
      : Container(
          height: height,
          width: width,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(15),
            image: DecorationImage(
                image: FileImage(controller.file), fit: BoxFit.fill),
          ),
          child: GestureDetector(
            onTap: () {
              controller.clearImage();
            },
            child: const Padding(
              padding: EdgeInsets.all(4),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  AppIcons(
                    icon: Icons.clear,
                    iconColor: AppColors.white,
                    backgruondcolor: Colors.red,
                    containerSize: 35,
                  ),
                ],
              ),
            ),
          ),
        );
}
