import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

class BigText extends StatelessWidget {
  Color? color;
  final String text;
  double size;
  TextOverflow overflow;
  String fontFamily;
  FontWeight fontWeight;
  TextDecoration textDecoration;
  int maxLine;
  BigText({
    Key? key,
    this.color = const Color(0xFFFFFFFF),
    required this.text,
    this.size = 0,
    this.overflow = TextOverflow.ellipsis,
    this.fontWeight = FontWeight.w400,
    this.fontFamily = 'Kalam-Bold',
    this.maxLine = 5,
    this.textDecoration = TextDecoration.none,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      overflow: overflow,
      maxLines: maxLine,
      style: TextStyle(
        decoration: textDecoration,
        color: color,
        fontWeight: fontWeight,
        fontFamily: fontFamily,
        fontSize: size == 0 ? 12.sp : size,
      ),
    );
  }
}
