import 'package:flutter/material.dart';

class SmallText extends StatelessWidget {
  Color? color;
  final String text;
  double size;
  double height;
  FontWeight fontWeight;
  String fontFamily;
  int maxLine;
  SmallText({
    Key? key,
    this.color=const Color(0xffffffff),
    required this.text,
    this.height = 1.2,
    this.size = 15,
    this.fontWeight = FontWeight.w400,
    this.fontFamily = 'Kalam-Light',
    this.maxLine = 5,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      maxLines: maxLine,
      style: TextStyle(
        color: color,
        fontWeight: fontWeight,
        fontFamily: fontFamily,
        fontSize: size,
        height: height,
        overflow: TextOverflow.ellipsis,
      ),
    );
  }
}
