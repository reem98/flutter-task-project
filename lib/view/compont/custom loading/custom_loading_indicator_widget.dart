import 'package:flutter/material.dart';

import 'package:loading_indicator/loading_indicator.dart';


class CustomLoadingIndicatorWidget extends StatelessWidget {
  final double heightOfRing;
  final Color color;

  const CustomLoadingIndicatorWidget({ this.heightOfRing = 70,
    this.color =const Color(0xFFFDAF56),Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return Center(
      child: SizedBox(
        height: heightOfRing,
        child: LoadingIndicator(
            indicatorType: Indicator.ballSpinFadeLoader,
            colors: [color],
            strokeWidth: 2,
            backgroundColor: Colors.transparent,
            pathBackgroundColor: Colors.black),
      ),
    );
  }
}