import 'package:flutter/material.dart';

import '../../../utils/colors.dart';

class InputTextFormField extends StatefulWidget {
  //IconData icon;
  String hintText;
  bool isPassword;
  bool isNumber;
  TextAlign? textAlign;
  TextStyle? textStyle;
  bool isCenter;
  double? height;
  double? iconSize;
  double? width;
  int? maxLine;
  TextEditingController textEditingController;
  InputTextFormField(
      {Key? key,
      this.isPassword = false,
      this.isNumber = false,
      this.isCenter = false,
      //  required this.icon,
      this.height = 0,
      this.width = 1,
      this.textAlign = TextAlign.start,
      this.textStyle,
      this.iconSize = 24,
      this.maxLine = 1,
      required this.hintText,
      required this.textEditingController})
      : super(key: key);

  @override
  State<InputTextFormField> createState() => _InputTextFormFieldState();
}

class _InputTextFormFieldState extends State<InputTextFormField> {
  bool show = true;
  @override
  Widget build(BuildContext context) {
    return Container(
      height: widget.height != 0 ? widget.height : null,
      alignment: widget.isCenter ? Alignment.center : null,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: Theme.of(context).brightness == Brightness.dark
            ? AppColors.mainColor
            : AppColors.mainColor2,
      ),
      child: TextFormField(
        style: widget.textStyle,
        textAlign: widget.textAlign!,
        keyboardType: widget.isNumber ? TextInputType.number : null,
        maxLines: widget.maxLine,
        obscureText: widget.isPassword ? show : false,
        autovalidateMode: AutovalidateMode.onUserInteraction,
        controller: widget.textEditingController,
        decoration: InputDecoration(
          suffixIcon: widget.isPassword
              ? GestureDetector(
                  onTap: () {
                    setState(() {
                      show = !show;
                    });
                  },
                  child: const Icon(
                    Icons.remove_red_eye_outlined,
                  ),
                )
              : null,
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10),
            borderSide: BorderSide(color: AppColors.white, width: 2),
          ),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10),
            borderSide: BorderSide(color: AppColors.mainColor2),
          ),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10),
            borderSide: BorderSide(color: AppColors.mainColor2),
          ),
          disabledBorder: null,
          hintText: widget.hintText,
          hintStyle: const TextStyle(color: AppColors.white),
        ),
      ),
    );
  }
}
