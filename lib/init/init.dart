import 'package:get/get.dart';
import 'package:task_project_flutter/service/repository/adding%20posts/adding_posts_repo.dart';
import '../logic/Controllers/adding posts/adding_posts_controller.dart';
import '../logic/Controllers/home posts/home_posts_controller.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../service/api/api_client.dart';
import '../service/repository/home posts/posts_repo.dart';
import '../utils/app_constants.dart';

Future<void> init() async {
  //shared
  final prefs = await SharedPreferences.getInstance();
  Get.lazyPut(() => prefs,fenix: true);

  //Api Client
  Get.lazyPut(() => ApiClient(main_BaseUrl: AppConstants.BASE_URL, sharedPre: Get.find()),fenix: true);

  //Repo
  Get.lazyPut(() => PostsRepo(apiClient: Get.find()),fenix: true);
  Get.lazyPut(() => AddingPostsRepo(),fenix: true);
 
  //Controller
  Get.lazyPut(() => HomePostsController(prefs: Get.find(),postsRepo: Get.find()),fenix: true); 
  Get.lazyPut(() => AddingPostsController(prefs: Get.find(),addingPostsRepo: Get.find()),fenix: true);
}
