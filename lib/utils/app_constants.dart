class AppConstants {
  /*  for app   */
  static const String APP_NAME = "Task Project";
  static const int APP_VERSION = 1;

  /*  for api   */

  //for base Uri
  static const String BASE_URL = "http://image-store.mhdanas.com";//http://online-free-course.000webhostapp.com
  static const String TOKEN = "";//http://online-free-course.000webhostapp.com

//for auth
  static const String LOGIN_URL = "/Login/CheckLogin";//http://online-free-course.000webhostapp.com
//for posts
  static const String GET_POPULAR_POSTS = "/api/posts/popular";
  static const String GET_ALL_POSTS = "/api/posts";
  static const String ADDING_POSTS = "/api/posts/create";


  //for shared prefrerense keys
   static const String IS_SHOWN_SPLASH = "splash";


}
