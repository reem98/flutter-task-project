import 'dart:ui';

class AppColors {
  static const Color textColor2 = Color(0xFF202020);
  static const Color hintColor = Color(0xff9d9d9d);
  static const Color innerShadow = Color(0xFF737373);
  static const Color dropShadow = Color(0xFF858585);
  static const Color border = Color(0xFF707070);
  static const Color orangeBorder = Color(0xDAFF9326);
  static const Color iconColor =Color(0xFF0075EA);
  static const Color green = Color(0xff62fe4a);
  static const Color red = Color(0xfff72228);
  static const Color blue = Color(0xff1ca9fa);
  static const Color bluegra = Color(0xff43e0ff);
  static const Color yellowgra = Color(0xffffdc00);
  static const Color orange = Color(0xfffdca98);
  static const Color orange2 = Color(0xfffde8d3);
  static const Color textorange = Color(0xffda6f06);
  static const Color orangebegin = Color(0xfffffb2d);
  static const Color orangeend = Color(0xffda063f);
  static const Color persinte = Color(0xfff86266);
  static const Color green0 = Color(0xffe7ff86);
  static const Color green01 = Color(0xfff3ffc6);
  static const Color green1 = Color(0xffe3ff6f);
  static const Color textGreen = Color(0xff809a00);
  static const Color green2 = Color(0xfff4ffc6);
  static const Color courseColor = Color(0xFF242424);
  static const Color black = Color(0xFF000000);
  static const Color purple = Color(0xFFA69CFD);
  ////////////////
  static const Color backGroundBottom = Color(0xFF8EC7FF);
  static const Color backGroundBottom2 = Color(0xFF8EC7FF);
  static const Color backGroundColor = Color(0xFFECF5FC);
  
  static const Color mainColor2 = Color(0xff6D9886);
  static const Color white = Color(0xffffffff);
  static const Color backGroundGray = Color(0xff979797);
  static const Color backGroundCard = Color(0xfff5f5f5);
  static const Color backGroundTextEdit = Color(0xfff5f5f5);

static const Color mainColor = Color(0xFF1B221F);
static const Color secondColor = Color(0xFF1C4A5A);
static const Color textColor = Color(0xFFFDAF56);

 
}
