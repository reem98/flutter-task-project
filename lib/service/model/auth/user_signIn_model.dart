class UserSingInModel {
  String? userName;
  String? password;

  UserSingInModel(this.userName, this.password);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['username'] = userName;
    data['password'] = password;
    return data;
  }
}
