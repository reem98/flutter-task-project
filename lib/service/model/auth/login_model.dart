class LoginModel {
  String? agentGuide;
  String? agentName;
  String? latinName;
  int? userLanguage;
  String? phone;
  String? email;
  String? fulladress;
  String? myToken;

  LoginModel(
      {this.agentGuide,
      this.agentName,
      this.latinName,
      this.userLanguage,
      this.phone,
      this.email,
      this.fulladress,
      this.myToken});

  LoginModel.fromJson(Map<String, dynamic> json) {
    agentGuide = json['agentGuide'];
    agentName = json['agentName'];
    latinName = json['latinName'];
    userLanguage = json['userLanguage'];
    phone = json['phone'];
    email = json['email'];
    fulladress = json['fulladress'];
    myToken = json['myToken'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['agentGuide'] = this.agentGuide;
    data['agentName'] = this.agentName;
    data['latinName'] = this.latinName;
    data['userLanguage'] = this.userLanguage;
    data['phone'] = this.phone;
    data['email'] = this.email;
    data['fulladress'] = this.fulladress;
    data['myToken'] = this.myToken;
    return data;
  }
}