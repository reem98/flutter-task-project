class AccountsModel {
  int? id;
  String? cardGuide;
  String? mainAccount;
  String? cardCode;
  String? accountName;
  String? latinName;

  AccountsModel(
      {this.id,
      this.cardGuide,
      this.mainAccount,
      this.cardCode,
      this.accountName,
      this.latinName});

  AccountsModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    cardGuide = json['cardGuide'];
    mainAccount = json['mainAccount'];
    cardCode = json['cardCode'];
    accountName = json['accountName'];
    latinName = json['latinName'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['cardGuide'] = this.cardGuide;
    data['mainAccount'] = this.mainAccount;
    data['cardCode'] = this.cardCode;
    data['accountName'] = this.accountName;
    data['latinName'] = this.latinName;
    return data;
  }
}