import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../../utils/app_constants.dart';
import '../../api/api_client.dart';
import '../../model/auth/login_model.dart';
import '../../model/auth/user_signIn_model.dart';

class AuthRepo extends GetxService {
  ApiClient apiClient;
  SharedPreferences sharedPreferences;

  AuthRepo({
    required this.apiClient,
    required this.sharedPreferences,
  });

  // Future<Response> getProfileInfo() async {
  //   return await apiClient.getData(AppConstants.USER_INFO);
  // }



  Future<Response> loginFunction(UserSingInModel userLoginModel) async {
    Response response = await apiClient.postData(
        AppConstants.LOGIN_URL, userLoginModel.toJson());
    return response;
  }

  saveUserToken(LoginModel loginModel) async {
    apiClient.token = loginModel.myToken.toString();

    await sharedPreferences.setString(
        AppConstants.TOKEN, loginModel.myToken.toString());
    apiClient.updateHeaders(loginModel.myToken.toString());
  }

 

  bool isAuth() {
    return sharedPreferences.containsKey(AppConstants.TOKEN);
  }


  String getToken() {
    return sharedPreferences.getString(AppConstants.TOKEN).toString();
  }

  Future<bool> clearUserAuth() async {
    if (await sharedPreferences.remove(AppConstants.TOKEN)) {
      await sharedPreferences.clear();
      apiClient.token = '';
      apiClient.updateHeaders('');
      return true;
    } else {
      return false;
    }
  }
}
