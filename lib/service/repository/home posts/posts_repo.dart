import 'package:get/get_connect/http/src/response/response.dart';
import 'package:get/get_state_manager/src/rx_flutter/rx_disposable.dart';
import '../../../utils/app_constants.dart';
import '../../api/api_client.dart';

class PostsRepo extends GetxService {
  late final ApiClient apiClient;

  PostsRepo({required this.apiClient});

  Future<Response> getPopularPosts() async {
    return await apiClient.getData(AppConstants.GET_POPULAR_POSTS);
  }

   Future<Response> getallPosts() async {
    return await apiClient.getData(AppConstants.GET_ALL_POSTS);
  }
  // Future<Response> showAd(IdModel id) async {
  //   return await apiClient.postData(
  //       AppConstants.AD_VIEW_URL, id.toJson());
  // }
}
