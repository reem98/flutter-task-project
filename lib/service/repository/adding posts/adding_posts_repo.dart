import 'dart:io';
import 'package:dio/dio.dart';
import 'package:get/get.dart' as getit;

import '../../../utils/app_constants.dart';

class AddingPostsRepo extends getit.GetxService {
  // late final ApiClient apiClient;

  // PostsRepo({required this.apiClient});

  // Future<Response> getPopularPosts() async {
  //   return await apiClient.getData(AppConstants.GET_POPULAR_POSTS);
  // }

  Future<Response> uploadPost({
    File? image,
    required String name,
    required String description,
  }) async {
    late FormData formData;
    formData = FormData.fromMap({
      "file": await MultipartFile.fromFile(image!.path,
          filename: image.path.split('/').last),
      "name": name,
      "description": description,
      "category_id":1,
      "user_id":1
    });

    Dio dio = Dio();
    return await dio.post(AppConstants.BASE_URL + AppConstants.ADDING_POSTS,
        data: formData,
        options: Options(
          headers: {
            "Content-Type": "application/json",
            "Authorization":
                "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiZWExMzBkYjcwNTdiZGM0MDdmZTkxMmMxNDcxNDY0ZGE1NzI2ZmM1MjRlODExZTczYzI0MDUxZGRjZjcyZjQwNTFhMDFjOGViZDgzZTdiMDAiLCJpYXQiOjE2OTY4MzY1OTcuNDE1OTQ2LCJuYmYiOjE2OTY4MzY1OTcuNDE1OTUyLCJleHAiOjE3Mjg0NTg5OTcuNDA0NjQsInN1YiI6IjEiLCJzY29wZXMiOltdfQ.olwDcQ62CIh8HK1piZzf_M-KQhsUKpH-4bxJVOlnW7IlVYBEoix21vf39tCP-WTz7Cd12DMCQlXuv0OwQBtQAmqDVbpuG6_518z0SvoqfurxB2TGE_TJE-MUdyfU-C7y_GPxfLSnedXUvi914p9oFtshOE3JtETT4f6AB7vVZUptMjrk_BY3IHNEPM6-MFxO3o4Utfiztys5teX9M5wH_2b0PLisGxWfft3PnMZ6bIQR3FHUCndGIblY_u6SGEFlfz1CmHZ4d8VUwVCDRCasZrrU23Ll3YkZs1sISrbZJJ63OgUByHRzmhlR8ZKmQsozvm87LKTLdDZjUlmwb7PPNSLykvxk_ZVckoAJSOyaU-53DcifsrNtmqT6hFtrwr7aZWTg0oMI6aO97PiiyPXnopTibHZD1wBRmhbQAldBR8RDJNkO2JMKAIWiTqZyFwoTs89i9YJDKAGIsrhzW81dufEgMEvaWlDG0CG0P2zqkhNA0mdXElVyAYlXtkwW5m4Ha1p3MJFSabBrO_4YEOJax1ACAhKD2pHsdv_ZQt28RaAUxCc83eIf1TnUcaG-NI6iHs-2lC2AAjlLEnSX4w8Ml6GY-vHyQUDyBGurh12Qcmtq_Jh9gYaDDS3UCDwQ6LS4O7ZvXg0470fXfWTY-TZCOqU6PKsiRuAM16JuwH-TMUw",
          },
          validateStatus: (_) => true,
          contentType: Headers.jsonContentType,
          responseType: ResponseType.json,
        ));
  }
}
