import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../utils/app_constants.dart';

class ApiClient extends GetConnect implements GetxService {
  late SharedPreferences sharedPre;
  final String? main_BaseUrl;
  late String token = "";
  late Map<String, String> _Main_Headers;

  ApiClient({required this.main_BaseUrl, required this.sharedPre}) {
    baseUrl = AppConstants.BASE_URL;
    timeout = const Duration(seconds: 30);
    token = sharedPre.getString(AppConstants.TOKEN) ?? "";
    _Main_Headers = {
      'Content-type': 'application/json; charset=UTF-8',
      'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiOGExNzExMTk4NTllZjRjYjhiNWUyNDZmZGEzY2I2MzllNmM3OGYxYWQ4NjQ2YjMwZjFmNDAxMTU4MDFjNmY5ZTBjMGY1NjQxNTg2OTRhMDgiLCJpYXQiOjE2OTU5MDUxNjEuMzAyNDI5LCJuYmYiOjE2OTU5MDUxNjEuMzAyNDM1LCJleHAiOjE3Mjc1Mjc1NjEuMjk3NDYzLCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.WigDxjOG7vL4iqJ3vAnFT4--P9nCykvIenqFThbgkYulavqKBlLMhvKHAxcNGzc4vj2pm2ch05vuFOPwI9vKORjD6gEmZdqjTXaTz6r3y60zigQOY6TGwdjqQogqb2WpndQxRqZbxex4FC5ikP6x7BV6HPW1wcfh7zXPfdx7qjzQQ2MUJ3iSSCV4ZrQsIxcXY43kNG-3s487gidVYRmDWxCuiBcKYibTvou4e7fDZ3L3aKud2R718Xu8g2emKz2buIO2WhSJ8t2oYOZa34F12RysfdgSGd2yRjfoptZDv03X3uLaKOdCyBexheem2J74kQ6mxjnit0ge_ptLP_a3srCBUz6qv0x7Cp6aQpUB9LCgqdiBxPulYGne6tGR9tDFm3VzqOaxBoLiW7OPsuClpxN3MGr-nvhnuyz2Uu6V4BOOWP0kZglPx2TUalhu72WarrWeD2HEFYFigKSaW-PAhYKImken4WNvMnUIv3ImfvnxVlGnxyXMIm3Ng2ko9bOniHst_29gISNRTcuvhXrluWePq-cIOZT_vSJ6E5kSpaWR-oP2F-0-fl1_YU4xm068qoqW-6FjJqAeMG3sdMsaiZ7PsxMS1_Tli4idzE65WrTjgGGA8yr8Dk87A6uSDuRz76ezhDMsmeVkhxSL6roMGa9ZWCYF__ZfhSmm6hWKHlM',
    };
  }

  void updateHeaders(String token) {
    _Main_Headers = {
      'Content-type': 'application/json; charset=UTF-8',
      'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiOGExNzExMTk4NTllZjRjYjhiNWUyNDZmZGEzY2I2MzllNmM3OGYxYWQ4NjQ2YjMwZjFmNDAxMTU4MDFjNmY5ZTBjMGY1NjQxNTg2OTRhMDgiLCJpYXQiOjE2OTU5MDUxNjEuMzAyNDI5LCJuYmYiOjE2OTU5MDUxNjEuMzAyNDM1LCJleHAiOjE3Mjc1Mjc1NjEuMjk3NDYzLCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.WigDxjOG7vL4iqJ3vAnFT4--P9nCykvIenqFThbgkYulavqKBlLMhvKHAxcNGzc4vj2pm2ch05vuFOPwI9vKORjD6gEmZdqjTXaTz6r3y60zigQOY6TGwdjqQogqb2WpndQxRqZbxex4FC5ikP6x7BV6HPW1wcfh7zXPfdx7qjzQQ2MUJ3iSSCV4ZrQsIxcXY43kNG-3s487gidVYRmDWxCuiBcKYibTvou4e7fDZ3L3aKud2R718Xu8g2emKz2buIO2WhSJ8t2oYOZa34F12RysfdgSGd2yRjfoptZDv03X3uLaKOdCyBexheem2J74kQ6mxjnit0ge_ptLP_a3srCBUz6qv0x7Cp6aQpUB9LCgqdiBxPulYGne6tGR9tDFm3VzqOaxBoLiW7OPsuClpxN3MGr-nvhnuyz2Uu6V4BOOWP0kZglPx2TUalhu72WarrWeD2HEFYFigKSaW-PAhYKImken4WNvMnUIv3ImfvnxVlGnxyXMIm3Ng2ko9bOniHst_29gISNRTcuvhXrluWePq-cIOZT_vSJ6E5kSpaWR-oP2F-0-fl1_YU4xm068qoqW-6FjJqAeMG3sdMsaiZ7PsxMS1_Tli4idzE65WrTjgGGA8yr8Dk87A6uSDuRz76ezhDMsmeVkhxSL6roMGa9ZWCYF__ZfhSmm6hWKHlM',
    };
  }

  Future<Response> getData(String uri, {Map<String, String>? headers}) async {
    SharedPreferences sharedPreferences = Get.find();
    if (sharedPreferences.getString(AppConstants.TOKEN).toString().length >
        40) {
      updateHeaders(sharedPreferences.getString(AppConstants.TOKEN).toString());
      try {
        Response response = await get(uri, headers: headers ?? _Main_Headers);
        return response;
      } catch (e) {
        return Response(statusCode: 1, statusText: e.toString());
      }
    } else {
      try {
        Response response = await get(uri, headers: headers ?? _Main_Headers);
        return response;
      } catch (e) {
        return Response(statusCode: 1, statusText: e.toString());
      }
    }
  }

  Future<Response> postData(String url, dynamic body) async {
    try {
      Response response = await post(
        url,
        body,
        headers: _Main_Headers,
      );
      return response;
    } catch (e) {
      print(e.toString());
      return Response(statusCode: 1, statusText: e.toString());
    }
  }
}
