import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:task_project_flutter/service/model/home%20posts/all_posts_model.dart';
import '../../../service/model/home posts/popular_slider_model.dart';
import '../../../service/repository/home posts/posts_repo.dart';
import '../../../view/compont/custom snackpar/show_custom_snackPar_red.dart';

class HomePostsController extends GetxController implements GetxService {
  bool _isLoadingPopular = false;
  get isLoadPopular => _isLoadingPopular;

  bool _isLoadingAll = false;
  get isLoadAll => _isLoadingAll;

  late SharedPreferences prefs;
  late PostsRepo postsRepo;

  late PopularSliderModel popularSliderModel;
  late AllPostsModel allPostsModel;

  HomePostsController({required this.prefs, required this.postsRepo});

  @override
  void dispose() async {
    super.dispose();
  }

  @override
  void onInit() {
    super.onInit();
    getAllPosts();
    getPopularPosts();
  }

  getPopularPosts() async {
    _isLoadingPopular = true;
    update();
    Response response = await postsRepo.getPopularPosts();
    if (response.statusCode == 200) {
      popularSliderModel = PopularSliderModel.fromJson(response.body);
      _isLoadingPopular = false;
      update();
    } else {
      showCustomSnackBarRed('Something weny wrong try again!', 'Error');
    }
  }

  getAllPosts() async {
    _isLoadingAll = true;
    update();
    Response response = await postsRepo.getallPosts();
    if (response.statusCode == 200) {
      allPostsModel = AllPostsModel.fromJson(response.body);
      var posted = [];
      posted.addAll(response.body['posts']);
      posted.addAll(allPostsModel.posts as Iterable);
      print(response.body);
      _isLoadingAll = false;
      update();
    } else {
      showCustomSnackBarRed('Something weny wrong try again!', 'Error');
    }
  }
}
