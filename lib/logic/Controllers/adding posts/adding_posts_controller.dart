import 'package:image_picker/image_picker.dart';
import 'dart:io';
import 'package:path/path.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:task_project_flutter/logic/Controllers/home%20posts/home_posts_controller.dart';
import 'package:task_project_flutter/service/repository/adding%20posts/adding_posts_repo.dart';
import 'package:task_project_flutter/view/compont/custom%20snackpar/show_custom_snackpar_green.dart';
import '../../../view/compont/custom snackpar/show_custom_snackPar_red.dart';
import '../../../view/sections widget in pages/adding posts page/show_bottom_sheet_image_widget.dart';

class AddingPostsController extends GetxController {
  late AddingPostsRepo addingPostsRepo;
  AddingPostsController({required this.prefs, required this.addingPostsRepo});

  bool _isLoading = false;
  get isLoad => _isLoading;
  late SharedPreferences prefs;

  late TextEditingController nameController;
  late TextEditingController descriptionController;

  late String imageName = '';
  late String imageUrl = '1';
  late File file;
  late PickedFile? image;

  @override
  void dispose() async {
    nameController.dispose();
    descriptionController.dispose();
    super.dispose();
  }

  @override
  void onInit() {
    nameController = TextEditingController();
    descriptionController = TextEditingController();
    super.onInit();
  }

  gallery() async {
    final imagePicker = ImagePicker();
    image = await imagePicker.getImage(source: ImageSource.gallery);
    if (image != null) {
      file = File(image!.path);
      imageName = basename(image!.path);
      Get.back();
    } else {
      Get.snackbar('Alert', 'no image selected');
    }
    update();
  }

  camera() async {
    final imagePicker = ImagePicker();
    PickedFile? image = await imagePicker.getImage(source: ImageSource.camera);
    if (image != null) {
      file = File(image.path);
      imageName = basename(image.path);
      Get.back();
    } else {
      Get.snackbar('Alert', 'no image selected');
    }
    update();
  }

  clearImage() {
    imageName = '';
    imageUrl = '1';
    file.delete();
    update();
  }

  requestPermisionForPhotos(dynamic controller, context) async {
    PermissionStatus galleryStatus = await Permission.storage.request();
    PermissionStatus cameraStatus = await Permission.camera.request();
    if (galleryStatus.isGranted && cameraStatus.isGranted) {
      showBottomSheetImageWidget(context, controller);
    } else {
      showCustomSnackBarRed('You need to give permission', 'Error');
    }
  }

  Future<void> uploadPost() async {
    String name = nameController.text.trim();
    String description = descriptionController.text.trim();
    if (name.isEmpty) {
      showCustomSnackBarRed('enter the name field', 'empty field');
    } else if (description.isEmpty) {
      showCustomSnackBarRed('enter the description ', 'empty field');
    } else if (imageName == '') {
      showCustomSnackBarRed('enter a image', 'empty field');
    } else {
      _isLoading = true;
      update();
      addingPostsRepo
          .uploadPost(
        image: file,
        name: name,
        description: description,
      )
          .then((value) async {
        if (value.statusCode == 200) {
          nameController.text = '';
          descriptionController.text = '';
          clearImage();
          showCustomSnackParGreen(value.data['msg'], 'Succeed');
          Get.find<HomePostsController>().getAllPosts();
          _isLoading = false;
          update();
        } else {
          showCustomSnackBarRed('check your internet connection', 'error');
          _isLoading = false;
          update();
        }
      });
    }
  }
}
