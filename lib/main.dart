import 'package:task_project_flutter/utils/app_constants.dart';

import '../routes/routes.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'init/init.dart';
import 'package:sizer/sizer.dart';

import 'logic/Controllers/home posts/home_posts_controller.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await init();

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    Get.find<HomePostsController>();
    return Sizer(
      builder: (context, orientation, deviceType) {
        return GetMaterialApp(
          debugShowCheckedModeBanner: false,
          getPages: AppRoutes.routes,
          initialRoute: Get.find<HomePostsController>()
                  .prefs
                  .containsKey(AppConstants.IS_SHOWN_SPLASH)
              ? AppRoutes.mainBottomNavigationBar
              : AppRoutes.splashscreen,
        );
      },
    );
  }
}
