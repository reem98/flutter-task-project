import 'package:get/get_navigation/get_navigation.dart';
import '../view/pages/home page/home_page.dart';
import '../view/pages/home page/main_navigation_bar_page.dart';
import '../view/pages/splash page/splash_page.dart';

class AppRoutes {
  static const splashscreen = Routes.splashscreen;
  static const homePage = Routes.homePage;
  static const mainBottomNavigationBar = Routes.mainBottomNavigationBar;

  static final routes = [
    GetPage(
        name: Routes.splashscreen,
        page: () => const SplashPage(),
        transition: Transition.fadeIn),
    GetPage(
        name: Routes.homePage,
        page: () => const HomePage(),
        transition: Transition.fadeIn),
    GetPage(
        name: Routes.mainBottomNavigationBar,
        page: () => const MainBottomNavigationBarPage(),
        transition: Transition.fadeIn),
  ];
}

class Routes {
  static const splashscreen = '/splashscreen';
  static const homePage = '/homePage';
  static const mainBottomNavigationBar = '/mainBottomNavigationBar';
}
